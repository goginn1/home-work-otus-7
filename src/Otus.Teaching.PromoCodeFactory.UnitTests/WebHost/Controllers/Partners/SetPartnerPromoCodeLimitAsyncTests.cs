﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace SetPartnerPromoCodeLinitAsync.Tests
{
    public class PartnersControllerTests
    {
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsInactive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var repositoryMock = new Mock<IRepository<Partner>>();
            var inactivePartner = TestDataFactory.CreateInactivePartner(partnerId);
            repositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(inactivePartner);
            var controller = new PartnersController(repositoryMock.Object);
            var request = new SetPartnerPromoCodeLimitRequest { Limit = 100, EndDate = DateTime.Now.AddDays(30) };
        
            // Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);
        
            // Assert
            result.Should().BeOfType<BadRequestObjectResult>()
                .Which.Value.Should().Be("Данный партнер не активен");
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var repositoryMock = new Mock<IRepository<Partner>>();
            repositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(TestDataFactory.GetNonExistentPartner());
            var controller = new PartnersController(repositoryMock.Object);
            var request = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 100,
                EndDate = DateTime.Now.AddDays(30)
            };
        
            // Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);
        
            // Assert
            result.Should().BeOfType<NotFoundResult>();
        }
        
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_ExistingActiveLimit_ResetsIssuedPromoCodesAndCancelsActiveLimitAndCreatesNewLimit()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var existingLimitId = Guid.NewGuid();
            var partner = TestDataFactory.CreatePartnerWithActiveLimit(partnerId, existingLimitId, 50, 100);

            var repositoryMock = new Mock<IRepository<Partner>>();
            repositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            repositoryMock.Setup(r => r.UpdateAsync(It.IsAny<Partner>())).Returns(Task.CompletedTask);

            var partnersController = new PartnersController(repositoryMock.Object);

            var request = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 200,
                EndDate = DateTime.Now.AddDays(10)
            };

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request) as CreatedAtActionResult;

            // Assert
            Assert.Equal(0, partner.NumberIssuedPromoCodes);
            Assert.NotNull(partner.PartnerLimits.FirstOrDefault(l => l.CancelDate != null));
            var newLimit = partner.PartnerLimits.FirstOrDefault(l => l.Id != existingLimitId);
            Assert.NotNull(newLimit);
            Assert.Equal(request.Limit, newLimit.Limit);
            Assert.Equal(request.EndDate, newLimit.EndDate);
            Assert.Null(newLimit.CancelDate);
            Assert.NotNull(result);
            Assert.Equal(nameof(PartnersController.GetPartnerLimitAsync), result.ActionName);
        }
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_LimitExpiredDoesNotResetIssuedPromoCodes_IfNoNewLimitSet()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var expiredLimitId = Guid.NewGuid();
            var issuedPromoCodes = 50; // Предположим, что уже выдано 50 промокодов
            var partner = new Partner
            {
                Id = partnerId,
                IsActive = true,
                NumberIssuedPromoCodes = issuedPromoCodes,
                PartnerLimits = new List<PartnerPromoCodeLimit>
                {
                    new PartnerPromoCodeLimit
                    {
                        Id = expiredLimitId,
                        PartnerId = partnerId,
                        Limit = 100,
                        CreateDate = DateTime.Now.AddDays(-10), // Дата создания 10 дней назад
                        EndDate = DateTime.Now.AddDays(-1), // Лимит истек вчера
                        CancelDate = null
                    }
                }
            };

            var repositoryMock = new Mock<IRepository<Partner>>();
            repositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            repositoryMock.Setup(r => r.UpdateAsync(It.IsAny<Partner>())).Returns(Task.CompletedTask);

            var controller = new PartnersController(repositoryMock.Object);

            // Act - не изменяем лимит, просто проверяем статус после истечения срока действия
            // Симулируем вызов, который мог бы проверить статусы или обновления, но не меняет лимит
            var result = await controller.GetPartnersAsync();

            // Assert
            Assert.Equal(issuedPromoCodes, partner.NumberIssuedPromoCodes); // Убеждаемся, что количество промокодов не изменилось
        }
    }
    
    public static class TestDataFactory
    {
        public static Partner CreatePartnerWithActiveLimit(Guid partnerId, Guid limitId, int issuedPromoCodes, int limit)
        {
            return new Partner
            {
                Id = partnerId,
                IsActive = true,
                NumberIssuedPromoCodes = issuedPromoCodes,
                PartnerLimits = new List<PartnerPromoCodeLimit>
                {
                    new PartnerPromoCodeLimit
                    {
                        Id = limitId,
                        PartnerId = partnerId,
                        Limit = limit,
                        CreateDate = DateTime.Now.AddDays(-1),
                        EndDate = DateTime.Now.AddDays(5),
                        CancelDate = null
                    }
                }
            };
        }
        
        public static Partner GetNonExistentPartner()
        {
            return null;
        }
        public static Partner CreateInactivePartner(Guid partnerId)
        {
            return new Partner
            {
                Id = partnerId,
                IsActive = false, 
                NumberIssuedPromoCodes = 0,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
        }
    }
    
}